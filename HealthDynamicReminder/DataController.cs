using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Reflection;
using HealthDynamicReminder.DB;
using HealthDynamicReminder.Models;
using MySql.Data.MySqlClient;

namespace HealthDynamicReminder
{
    public class DataController
    {
     
        
        public List<MailInterval> GetData(string interval)
        {

            var dateTime = DateTime.Now;
            var aboveTime = dateTime.AddMinutes(-15).ToString("H:mm:ss");
            var belowTime = dateTime.ToString("H:mm:ss");
            var currentDate = dateTime.ToString("yy-MM-dd");
            var dayOfWeek = dateTime.DayOfWeek.GetHashCode() == 0 ? 6 : (dateTime.DayOfWeek.GetHashCode() - 1); 
            
            var daysInMonth = DateTime.DaysInMonth(Int32.Parse(dateTime.ToString("yyyy")),Int32.Parse(dateTime.ToString("MM")));
            var dayOfMonth = Int32.Parse(dateTime.ToString("dd"));
            var days = "";
            if (dayOfMonth == daysInMonth && dayOfMonth < 31)
            {
                var day = dayOfMonth;
                do
                {
                    days += day+",";
                    day++;
                } while (day != 31);

                days += 31;
            }
            else
            {
                days += dayOfMonth;
            }
            
            var sql = $"SELECT m.id, u.email, mgt.title  FROM measure_mail_interval AS m " +
                      $"LEFT JOIN user AS u " +
                      $"ON m.user_id = u.id " +
                      $"LEFT JOIN measure_group_translation AS mgt " +
                      $"ON m.measure_group_id = mgt.translatable_id " +
                      $"WHERE m.time < '{belowTime}' " +
                      $"AND m.time > '{aboveTime}' " +
                      $"AND m.is_active = 1 " +
                      $"AND m.last_sent != '{currentDate}' " +
                      $"AND mgt.locale = 'uk' ";

            sql = interval switch
            {
                MailInterval.DAY => (sql + $"AND m.mailing_interval = 'day' "),
                MailInterval.WEEK => (sql + $"AND m.mailing_interval = 'week' " +
                                      $"AND m.day = {dayOfWeek} "),
                MailInterval.MONTH => (sql + $"AND m.mailing_interval = 'month' " +
                                       $"AND m.day IN ({days}) "),
                _ => sql
            };

            var mailIntervals = new List<MailInterval>();
            
            var c = DBConnection.Instance();
            if (c.IsConnected())
            {
                MySqlCommand mySqlCommand = new MySqlCommand(sql,c.Connection);
                MySqlDataReader rdr = mySqlCommand.ExecuteReader();
                while (rdr.Read())
                {
                    var mailInterval = new MailInterval();
                    mailInterval.UserEmail = rdr.GetString("email");
                    mailInterval.MeasureGroupTitle = rdr.GetString("title");
                    mailInterval.IntervalId = rdr.GetInt32("id");
                    mailIntervals.Add(mailInterval);
                    Console.WriteLine(mailInterval.ToString());
                }
                rdr.Close();
            }
            return mailIntervals;
        }
        
        public bool NotifyUser(string body, string to)
        {
            
            //TODO: Make record in database to notify user
            // var message = new MailMessage();
            // message.From = new MailAddress("drmxtmoi@gmail.com");
            // message.To.Add(to);
            // message.Subject = "sub";
            // message.IsBodyHtml = true;
            // message.Body = body;
            //
            // var smtpClient = new SmtpClient();
            // smtpClient.UseDefaultCredentials = true;
            // smtpClient.Host = "smtp.gmail.com";
            // smtpClient.Port = 465;
            // smtpClient.EnableSsl = true;
            // smtpClient.Credentials = new System.Net.NetworkCredential("drmxtmoi@gmail.com", "moi12345678");
            // try
            // {
            //  smtpClient.Send(message);
            //  return true;
            // }
            // catch (Exception e)
            // {
            //     Console.WriteLine(e);
            //     return false;
            // }
            return false;
        }

        public string ReadHtml(MailInterval mailInterval)
        {
            try
            {
                var path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"views/EmailLayout.html");
                var body = System.IO.File.ReadAllText(path);
                body =  body.Replace("@{body}", mailInterval.MeasureGroupTitle);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return null;
        }
        
        public void ChangeLastSend(MailInterval mailInterval)
        {
           
           var dateTime = DateTime.Now;
           var currentDate = dateTime.ToString("yy-MM-dd");
           var sql = $"UPDATE measure_mail_interval as mmi " +
                        $"SET last_sent = '{currentDate}' " +
                        $"WHERE mmi.id = {mailInterval.IntervalId};";

           var c = DBConnection.Instance();
            if (c.IsConnected())
            {
                MySqlCommand mySqlCommand = new MySqlCommand(sql,c.Connection);
                int rdr = mySqlCommand.ExecuteNonQuery();
                Console.WriteLine("change " + rdr);
            }
        }

        public void ManageData(List<MailInterval> mailIntervals)
        { 
            foreach (var mailInterval in mailIntervals)
            {
                // notifyUser();
                ChangeLastSend(mailInterval);
            }
        }
    }
}