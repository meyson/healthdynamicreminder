using System;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace HealthDynamicReminder.DB
{
    public class DBConnection
    {
        private const string Database = "db_moi";
        private const string Host = "localhost";
        private const string User = "root";
        private const string password = "1111";

        private DBConnection()
        {
        }
        

        private MySqlConnection connection = null;
        public MySqlConnection Connection
        {
            get { return connection; }
        }

        private static DBConnection _instance = null;
        public static DBConnection Instance()
        {
            if (_instance == null)
                _instance = new DBConnection();
            return _instance;
        }

        public void Connect()
        {
            string connstring = string.Format($"Server={Host}; database={Database}; UID={User}; password={password}");
            connection = new MySqlConnection(connstring);
            connection.Open();
        }
        public bool IsConnected()
        {
            if (Connection == null)
            {
                if (String.IsNullOrEmpty(Database) || String.IsNullOrEmpty(Host) || String.IsNullOrEmpty(User))
                    return false;
                Connect();
            }

            return true;
        }

        public void Close()
        {
            connection.Close();
        }    
    }
}