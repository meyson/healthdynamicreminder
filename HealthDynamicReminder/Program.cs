﻿using System;
using System.Collections.Generic;
using System.Threading;
using HealthDynamicReminder.DB;
using HealthDynamicReminder.Models;

namespace HealthDynamicReminder
{
    class Program
    {
        static void Main(string[] args)
        {
            var dataController = new DataController();
            
            while (true)
            {
                var data = dataController.GetData(MailInterval.DAY);
                dataController.ManageData(data);
                data = dataController.GetData(MailInterval.WEEK);
                dataController.ManageData(data);
                data = dataController.GetData(MailInterval.MONTH);
                dataController.ManageData(data);
                Thread.Sleep(10000); 
            }
        }
    }
}
