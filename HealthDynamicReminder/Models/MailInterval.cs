using System;
using System.Configuration;
using System.Net.Mail;

namespace HealthDynamicReminder.Models
{
    public class MailInterval
    {
        
        public const string DAY = "day";
        public const string MONTH = "month";
        public const string WEEK = "week";
        public const string MEASURE_GROUP_URL = "https://moi.health/uk/cabinet/my-health-dynamic/health-dynamics/";
        private int intervalId;
        private string userEmail;
        private string measureGroupTitle;

        public MailInterval()
        {
        }

        public MailInterval(int intervalId, string userEmail, string measureGroupTitle)
        {
            this.intervalId = intervalId;
            this.userEmail = userEmail;
            this.measureGroupTitle = measureGroupTitle;
        }

        public int IntervalId
        {
            get => intervalId;
            set => intervalId = value;
        }

        public string UserEmail
        {
            get => userEmail;
            set => userEmail = value;
        }

        public string MeasureGroupTitle
        {
            get => measureGroupTitle;
            set => measureGroupTitle = value;
        }

        public override string ToString()
        {
            return "id: " + intervalId + "\n userEmail: " + userEmail + "\n measureGroupTitle: " + measureGroupTitle + "\n";
        }
    }
}